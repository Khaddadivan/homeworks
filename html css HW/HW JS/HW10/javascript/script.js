`use strict`


const tabs = document.querySelectorAll(".tabs-title");
const contents = document.querySelectorAll(".tabs-content li");


tabs.forEach(tab => {

    tab.addEventListener("click", function () {

        contents.forEach(content => {

            content.style.display = "none";
        });

    const attrTad = tab.getAttribute("data-tab");

    const tabContent = document.querySelector(`[data-textContent = "${attrTad}"]`);

    if(tabContent) {

        tabContent.style.display = "block";

    }

    tabs.forEach(tab => {

        tab.classList.remove("active")

    });

    tab.classList.add("active");

    });

})
