`use strict`


const inputs = document.querySelectorAll(".user-password");

const icons = document.querySelectorAll(".fa-eye");

icons.forEach((icon, index) => {

    icon.addEventListener("click", () => {

        const input = inputs[index];

        togglePasswordVisibility(input, icon);

    });

});

function togglePasswordVisibility(input, icon) {

    if (input.type === "password") {

        input.type = "text";

        icon.classList.remove("fa-eye");

        icon.classList.add("fa-eye-slash");

    } else if (input.type === "text") {

        input.type = "password";

        icon.classList.remove("fa-eye-slash");

        icon.classList.add("fa-eye");

    }
}


function checkPasswords() {

    const password1 = document.getElementById("password1").value;

    const password2 = document.getElementById("password2").value;

    if (password1 === password2) {

        alert("You are welcome");

        const errorText = document.querySelector(".error-text");

        if (errorText) {

            errorText.style.display = "none";

        }

    } else {

        const errorText = document.querySelector(".error-text");

        if (errorText) {

            errorText.style.display = "block";

        } else {

            const errorText = document.createElement("p");

            errorText.textContent = "Потрібно ввести однакові значення";

            errorText.style.color = "red";

            errorText.classList.add("error-text");

            document.body.appendChild(errorText);

        }
    }
}
