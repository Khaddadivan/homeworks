'use strict';

const buttons = document.querySelectorAll('.btn');

document.addEventListener('keyup', (event) => {

    let key = event.key.toUpperCase();

    if (key === 'ENTER') {

        key = 'Enter';

    }

    buttons.forEach((button) => {

        button.style.backgroundColor = "";

    });

    buttons.forEach((button) => {

        if (button.getAttribute('data-key') === key) {

            button.style.backgroundColor = "blue";

        }

    });

});

