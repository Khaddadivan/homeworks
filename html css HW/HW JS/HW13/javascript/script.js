`use strict`


// 1.Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
// 2.Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
// 3.Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?

// 1. setTimeout() -  запускає функцію через заданий нами проміжок часу 1 раз;
// setInterval() - запускає функцію безліч разів за заданим нами інтервалом в часі.

// 2. Якщо встановити нульову затримку функція миттєво не спрацює вона спрацює лише тоді коли поточний код завершить виконнання.

// 3. для того, щоб цикл виконання не виконувався постійно якщо нам це вже не треба і не грузив зайвий раз процесор
// та пам'ять комп'ютера, ну і щоб новий цикл не запускався під час виконання старого циклу.
document.addEventListener("DOMContentLoaded", function () {


    const images = document.querySelectorAll(".image-to-show");

    const startBtn = document.getElementById("startButton");

    const stopBtn = document.getElementById("stopButton");

    let currentIndex = 0;

    let intervalId;

    function showNextImage() {

        images[currentIndex].style.display = "none";

        currentIndex = (currentIndex + 1) % images.length;

        images[currentIndex].style.display = "block";
    }


    startBtn.addEventListener("click", function () {

        if (!intervalId) {

            intervalId = setInterval(showNextImage, 3000);
        }
    });

    stopBtn.addEventListener("click", function () {

        if (intervalId) {

            clearInterval(intervalId);

            intervalId = null;

        }

    });

})