`use strict`


const themeToggle = document.getElementById('theme-toggle');

const body = document.body;

let currentTheme = localStorage.getItem('theme');


if (currentTheme === 'dark') {

    body.classList.add('page-body-dark');

} else {

    body.classList.add('page-body-light');

}


themeToggle.addEventListener('click', () => {


    if (currentTheme === 'light') {

        currentTheme = 'dark';

        body.classList.remove('page-body-light');

        body.classList.add('page-body-dark');

    } else {

        currentTheme = 'light';

        body.classList.remove('page-body-dark');

        body.classList.add('page-body-light');

    }


    localStorage.setItem('theme', currentTheme);

});