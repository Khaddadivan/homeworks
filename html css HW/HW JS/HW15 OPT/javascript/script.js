`use strict`

const factorialNum = n => (n === 0 || n === 1 ? 1 : n * factorialNum(n - 1));

let userInputNumber = 0;

do {
   userInputNumber = +prompt("enter your number ");
   if (isNaN(userInputNumber)){
       alert("enter your number, correct!")
   }

} while (isNaN(userInputNumber));

const result = factorialNum(userInputNumber);
console.log(`factorial of ${userInputNumber}: ${result} `);