`use strict`

const student = {
    name: "",

    lastName: "",

    tabel: {}
};


student.name = prompt("enter your name:");
student.lastName = prompt("enter your lastname:");


while (true) {

    const subject = prompt("enter name of subject:");

    if (subject === null || subject === "") {

        break;

    }

    const grade = +prompt(`enter your grade ${subject}:`);

    if (!isNaN(grade) && grade !== "") {

        student.tabel[subject] = grade;

    } else {

        alert("enter your grade, correct!");

    }
}


let lowGradesCount = 0;

let totalGrade = 0;

let subjectsCount = 0;


for (const subject in student.tabel) {

    const grade = student.tabel[subject];

    if (grade < 4) {

        lowGradesCount++;
    }

    totalGrade += grade;

    subjectsCount++;
}

const averageGrade = totalGrade / subjectsCount;


if (lowGradesCount > 0) {

    console.log(`total low grade: ${lowGradesCount}`);

} else {

    console.log("Студент переведено на наступний курс");
}

console.log("Середній бал: " + averageGrade);

if (averageGrade > 7) {

    console.log("Студенту призначено стипендію");
}

