`use strict`


let userName = prompt('enter your name');
let userAge = +prompt('enter your age');


while (userName === "" || isNaN(userAge) && userAge === "") {
    userName = prompt('Please enter your name correctly!');
    userAge = +prompt('Please enter your age with a number!');
}

if (userAge < 18) {
    alert('You are not allowed to visit this website');
} else if (userAge >= 18 && userAge <= 22) {
    const confirmed = confirm("Are you sure you want to continue?");
    if (confirmed) {
        alert("Welcome " + userName);
    } else {
        alert("You are not allowed to visit this website");
    }
} else {
    alert("Welcome " + userName);
}
