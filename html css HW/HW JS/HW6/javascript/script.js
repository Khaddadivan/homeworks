"use strict";

function createNewUser() {
    let userName = prompt("Enter your name");
    let userLastName = prompt("Enter your lastname");
    let userBirthday = prompt("Enter your birthday (dd.mm.yyyy)");

    let newUser = {
        firstName: userName,
        lastName: userLastName,
        birthday: formatDate(userBirthday),
        getLogin: function () {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
        getAge: function () {
            const today = new Date();
            const birthDate = this.birthday;
            let age = today.getFullYear() - birthDate.getFullYear();

            const birthMonth = birthDate.getMonth();
            const birthDay = birthDate.getDate();
            const todayMonth = today.getMonth();
            const todayDay = today.getDate();

            if (
                birthMonth > todayMonth || (birthMonth === todayMonth && birthDay > todayDay)) {
                age--;
            }

            return age;
        },

        getPassword: function () {
            return (
                this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear());
        },
    };

    return newUser;
}

function formatDate(dateString) {
    const parts = dateString.split(".");
    const day = parseInt(parts[0]);
    const month = parseInt(parts[1]) - 1;
    const year = parseInt(parts[2]);
    return new Date(year, month, day);
}





let newUser = createNewUser();

console.log(newUser);
console.log(newUser.getLogin());
console.log("Age: " + newUser.getAge());
console.log("Password: " + newUser.getPassword());
