`use strict`

function filterBy (arr, types) {
    let filteredArr = [];
    arr.forEach(function (item){
        if (typeof item !== types) {
            filteredArr.push(item);
        }
    });
    return filteredArr;
}
let data = ['hello', 'world', 23, '23', null];
let filteredData = filterBy(data, 'string');
console.log(filteredData);