`use strict`

let paragraphs = document.getElementsByTagName('p');
for (let i = 0; i < paragraphs.length; i++) {
    paragraphs[i].style.backgroundColor = "#ff0000";
}

let optionList = document.getElementById('optionsList');

console.log(optionList);

let parentElement = optionList.parentElement;

console.log(parentElement);

let childNodes = optionList.childNodes;

console.log(childNodes);

for (let i = 0; i < childNodes.length; i++) {
    let node = childNodes[i];
    console.log(node.nodeName, node.nodeType);
}

const testParagraph = document.querySelector('.options-list-text');
testParagraph.textContent = 'This is a paragraph';

let mainHeaderElements =  document.querySelectorAll(".main-header");
console.log(mainHeaderElements);
mainHeaderElements.forEach((elem) =>{
    elem.classList.add = ("nav-item");
    console.log(elem)
});

let sectionTitleElem = document.getElementsByClassName("section-title");
for (let i = 0 ; i < sectionTitleElem; i++) {
    sectionTitleElem[0].classList.remove("section-title");
}
console.log(sectionTitleElem);
