`use strict`


function Filterlist (array, parentDom = document.body) {
    let ul = document.createElement("ul");
    parentDom.append(ul);

    for (let arrayItem of array) {
        let li = document.createElement("li");
        ul.append(li);
    li.textContent = arrayItem;
    }
}
Filterlist(["1", "2", "3", "sea", "user", 23]);
Filterlist(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);